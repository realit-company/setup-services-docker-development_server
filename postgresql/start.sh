#!/bin/bash

if [ -z "$1" ]
then
      DOCKERAPP_ROOT=$PWD
else
      DOCKERAPP_ROOT=$1
fi

docker-compose -f $DOCKERAPP_ROOT/postgresql/docker-compose.yml stop && \
docker-compose -f $DOCKERAPP_ROOT/postgresql/docker-compose.yml rm -f

FILE=$DOCKERAPP_ROOT/postgresql/build/Dockerfile
if [ -f $FILE ]; then
  rm $FILE
fi
cat > $FILE <<EOL
FROM postgres:10.10

# RUN usermod -u ${UID:-0} postgres
EOL

docker-compose -f $DOCKERAPP_ROOT/postgresql/docker-compose.yml up -d --build
