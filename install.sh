#!/bin/bash

if [ -z "$1" ]
then
    DOCKERAPP_ROOT=$PWD
else
    DOCKERAPP_ROOT=$1
fi

CONFIG_DEV=$DOCKERAPP_ROOT/config/
CONFIG_PROD=$DOCKERAPP_ROOT/config-production/

if [ -d $CONFIG_PROD ]; 
then
  mv $DOCKERAPP_ROOT/config/ $DOCKERAPP_ROOT/config.bak/
  mv $DOCKERAPP_ROOT/config-production/ $DOCKERAPP_ROOT/config/
  PRODUCTION='true'
else
  PRODUCTION='false'
fi

echo '> SETUP: PORTAINER'
docker-compose -f $DOCKERAPP_ROOT/portainer/docker-compose.yml up -d

echo '> SETUP: HEIMDALL'
docker-compose -f $DOCKERAPP_ROOT/heimdall/docker-compose.yml up -d

echo '> SETUP: MARIADB + PHPMYADMIN'
$DOCKERAPP_ROOT/mariadb/start.sh $DOCKERAPP_ROOT
docker-compose -f $DOCKERAPP_ROOT/phpmyadmin/docker-compose.yml up -d

echo '> SETUP: MONGO + MONGO-EXPRESS'
docker-compose -f $DOCKERAPP_ROOT/mongo/docker-compose.yml up -d
docker-compose -f $DOCKERAPP_ROOT/mongo-express/docker-compose.yml up -d

echo '> SETUP: GLANCES'
docker-compose -f $DOCKERAPP_ROOT/glances/docker-compose.yml up -d

echo '> SETUP: POSTGRESQL + PGADMIN'
$DOCKERAPP_ROOT/postgresql/start.sh $DOCKERAPP_ROOT
docker-compose -f $DOCKERAPP_ROOT/pgadmin/docker-compose.yml up -d

if [ $PRODUCTION = 'true' ]; then
    rm -rf $DOCKERAPP_ROOT/config
    mv $DOCKERAPP_ROOT/config.bak $DOCKERAPP_ROOT/config
fi