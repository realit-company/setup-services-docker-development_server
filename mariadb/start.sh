#!/bin/bash

if [ -z "$1" ]
then
      DOCKERAPP_ROOT=$PWD
else
      DOCKERAPP_ROOT=$1
fi

docker-compose -f $DOCKERAPP_ROOT/mariadb/docker-compose.yml stop && \
docker-compose -f $DOCKERAPP_ROOT/mariadb/docker-compose.yml rm -f

FILE=$DOCKERAPP_ROOT/mariadb/build/Dockerfile
if [ -f $FILE ]; then
  rm $FILE
fi
cat > $FILE <<EOL
FROM mariadb:10

# RUN usermod -u ${UID:-0} mysql

COPY my.cnf /etc/mysql/my.cnf
EOL

docker-compose -f $DOCKERAPP_ROOT/mariadb/docker-compose.yml up -d --build

